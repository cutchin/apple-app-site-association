package com.cafex.apple;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet(urlPatterns = {"/", "/distros/"})
public class ContentServlet extends javax.servlet.http.HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String ROOT_FOLDER = "/opt/applications/";
//	private static final String ROOT_FOLDER = "/tmp/applications/";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		InputStream stream = null;
		try {
			stream = prepareResponse(request, response);
		} catch (IOException e) {
			System.out.println("ContentServlet: Could not open stream to file!" + e.getMessage());
		}

		if (stream == null) {
			System.out.println("ContentServlet: Could not open stream to file!");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		OutputStream output = response.getOutputStream();

		int read = 0;
		final byte[] bytes = new byte[1024];

		while ((read = stream.read(bytes)) != -1) {
			output.write(bytes, 0, read);
		}
	}

	private InputStream prepareResponse(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (request.getServletPath().equals("/")) {
			// Content of content.json should be returned from the root URL
			response.setContentType("application/json");
			return new FileInputStream(new File(ROOT_FOLDER + "content.json"));
		}

		if (!request.getServletPath().startsWith("/distros/")) {
			System.out.println("ContentServlet: distros missing from request");
			return null;
		}

		// Return the binary distros
		String distroName = request.getServletPath().substring(request.getServletPath().lastIndexOf("/") + 1);
		File distroFile = new File(ROOT_FOLDER + distroName);
		InputStream stream = new FileInputStream(distroFile);

		response.setContentLength((int) distroFile.length());

		if (distroName.endsWith("apk")) {
			// This is an android app
			response.setContentType("application/vnd.android.package-archive");
		} else if (distroName.endsWith("ipa")) {
			// This is an apple app
			response.setContentType("application/octet-stream .ipa");
		} else {
			System.out.println("ContentServlet: Only ipa and apk files are supported");
			return null;
		}

		return stream;
	}
}
